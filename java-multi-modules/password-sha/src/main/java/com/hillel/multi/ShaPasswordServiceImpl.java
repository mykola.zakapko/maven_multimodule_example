package com.hillel.multi;

import org.apache.commons.codec.digest.DigestUtils;

public class ShaPasswordServiceImpl implements IPasswordService {
    @Override
    public String hash(String input) {
        return DigestUtils.sha256Hex(input);
    }

    @Override
    public String algorithm() {
        return "sha256";
    }
}
