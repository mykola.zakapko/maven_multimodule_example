package com.hillel.multi;

public class App 
{
    public static void main(String[] args) {
        if (args.length > 0) {
            IPasswordService md5PasswordService = new Md5PasswordServiceImpl();
            System.out.println(md5PasswordService.algorithm() + " кодировка: "
                    + md5PasswordService.hash(args[0]));
            IPasswordService shaPasswordService = new ShaPasswordServiceImpl();
            System.out.println(shaPasswordService.algorithm() + " кодировка: "
                    + shaPasswordService.hash(args[0]));
        } else {
            System.err.println("Invalid input data: String[] args is empty!");
        }
    }

}
