package com.hillel.multi;


public interface IPasswordService
{
    String hash(String input);
    String algorithm();
}
